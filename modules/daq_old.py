import sys
from PyQt4 import QtCore, QtGui, uic
import threading

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.mime.base import MIMEBase
from email import encoders

from mcculw import ul
from mcculw.ul import ULError
from mcculw.enums import DigitalIODirection
import util
from ai import AnalogInputProps
from digital import DigitalProps

import pandas as pd
import matplotlib.pyplot as plt
import time

class WorkerObject(QtCore.QObject):
    channel_number = 0
    warning_charge = 4.1
    warning_discharge = 3.1
    
    channel_status = ['OFF']*7 # status: ON or OFF
    meas_type = ['']*7  # charge or discharge
    date_channel=['']*7   # date : Y-m-d - H-M-S - we add it to filename 
    sampleID=['']*7 
    index=[0]*7
    
    board_num = 0
    filename = ''
    
    
    ai_props = AnalogInputProps(board_num)
    ai_range = ai_props.available_ranges[0]
    
    digital_props = DigitalProps(board_num)
    port = next(
        (port for port in digital_props.port_info
         if port.supports_output), None)
#    ul.d_config_port(board_num, port.type, DigitalIODirection.OUT)
    port_value = 0xFF
    ul.d_out(board_num, port.type, port_value)
    bit_num = 0
    bit_value = 0
#    first_digital_output_turn_on_flag = 0
    
    started = False
    timer = QtCore.QTimer()
    updateLabel = QtCore.Signal(int,float)
    updatePushButton = QtCore.Signal(int)
    finished = QtCore.Signal()
    # timer.timeout.connect(get_read)
    # timer.start(1000)
    # def run(self):
        # While button_status
        # a_in(self.board_num,self.channel_number)
        
    def trigger_start_new(self):
        if not self.started:
            self.createDataFrame()                                        #
            self.started=True

            def running():
                self.get_read()
            t=threading.Thread(target=running)
            t.start()
                
#        while self.started is True:
#            self.timer.timeout.connect(self.get_read)
#            self.timer.start(1000)
#            self.get_read()
    def get_read(self):
        start_time = time.time()
        while self.started is True:
            if self.channel_status[0] == 'ON':
                self.measure(0)
                ch0_read_time = time.time()-start_time
                ch0_read = self.eng_units_value
                self.appendDataFrame(channel_num=0,read_value=ch0_read,read_time=ch0_read_time,sampleID=self.sampleID[0],date=self.date_channel[0],meas_type=self.meas_type[0],i=self.index[0])
                self.setDigitalOutputVoltage(channel_number=0,read_value=ch0_read,meas_type=self.meas_type[0])    
                self.updateLabel.emit(0,ch0_read)
                self.index[0]=self.index[0]+1
            if self.channel_status[1] == 'ON':
                self.measure(1)
                ch1_read_time = time.time()-start_time
                ch1_read = self.eng_units_value
                self.appendDataFrame(channel_num=1,read_value=ch1_read,read_time=ch1_read_time,sampleID=self.sampleID[1],date=self.date_channel[1],meas_type=self.meas_type[1],i=self.index[1])                
                self.setDigitalOutputVoltage(channel_number=1,read_value=ch1_read,meas_type=self.meas_type[1])
                self.updateLabel.emit(1,ch1_read)
                self.index[1]=self.index[1]+1
            if self.channel_status[2] == 'ON':
                self.measure(2)
                ch2_read_time = time.time()-start_time
                ch2_read = self.eng_units_value
                self.appendDataFrame(channel_num=2,read_value=ch2_read,read_time=ch2_read_time,sampleID=self.sampleID[2],date=self.date_channel[2],meas_type=self.meas_type[2],i=self.index[2])                
                self.setDigitalOutputVoltage(channel_number=2,read_value=ch2_read,meas_type=self.meas_type[2])
                self.updateLabel.emit(2,ch2_read)
                self.index[2]=self.index[2]+1
            if self.channel_status[3] == 'ON':
                self.measure(3)
                ch3_read_time = time.time()-start_time
                ch3_read = self.eng_units_value
                self.appendDataFrame(channel_num=3,read_value=ch3_read,read_time=ch3_read_time,sampleID=self.sampleID[3],date=self.date_channel[3],meas_type=self.meas_type[3],i=self.index[3])                
                self.setDigitalOutputVoltage(channel_number=3,read_value=ch3_read,meas_type=self.meas_type[3])
                self.updateLabel.emit(3,ch3_read)
                self.index[3]=self.index[3]+1
            if self.channel_status[4] == 'ON':
                self.measure(4)
                ch4_read_time = time.time()-start_time
                ch4_read = self.eng_units_value
                self.appendDataFrame(channel_num=4,read_value=ch4_read,read_time=ch4_read_time,sampleID=self.sampleID[4],date=self.date_channel[4],meas_type=self.meas_type[4],i=self.index[4])
                self.setDigitalOutputVoltage(channel_number=4,read_value=ch4_read,meas_type=self.meas_type[4])
                self.updateLabel.emit(4,ch4_read)
                self.index[4]=self.index[4]+1
            if self.channel_status[5] == 'ON':
                self.measure(5)
                ch5_read_time = time.time()-start_time
                ch5_read = self.eng_units_value 
                self.appendDataFrame(channel_num=5,read_value=ch5_read,read_time=ch5_read_time,sampleID=self.sampleID[5],date=self.date_channel[5],meas_type=self.meas_type[5],i=self.index[5])
                self.setDigitalOutputVoltage(channel_number=5,read_value=ch5_read,meas_type=self.meas_type[5])
                self.updateLabel.emit(5,ch5_read)
                self.index[5]=self.index[5]+1
            if self.channel_status[6] == 'ON':
                self.measure(6)
                ch6_read_time = time.time()-start_time
                ch6_read = self.eng_units_value
                self.setDigitalOutputVoltage(channel_number=6,read_value=ch6_read,meas_type=self.meas_type[6])
                self.setDigitalOutputVoltage(channel_number=5,read_value=ch5_read,meas_type=self.meas_type[5])
                self.appendDataFrame(channel_num=6,read_value=ch6_read,read_time=ch6_read_time,sampleID=self.sampleID[6],date=self.date_channel[6],meas_type=self.meas_type[6],i=self.index[6])
                self.updateLabel.emit(6,ch6_read)
                self.index[6]=self.index[6]+1
            
            time.sleep(1)
        self.finished.emit()
    def setDigitalOutputVoltage(self, channel_number,read_value,meas_type):
        
        if meas_type == 'charge':
            warning_voltage = self.warning_charge
            if read_value > warning_voltage:
                ul.d_bit_out(self.board_num, self.port.type, channel_number, 1)
                self.channel_status[channel_number] = 'OFF'
                print('Voltage over {} - measurement stopped'.format(warning_voltage))
                self.updatePushButton.emit(channel_number)
                self.sendMail(channel_number,warning_voltage)
        else:
            warning_voltage = self.warning_discharge 
            if read_value < warning_voltage:
                ul.d_bit_out(self.board_num, self.port.type, channel_number, 1)
                self.channel_status[channel_number] = 'OFF'
                print('Voltage under {} - measurement stopped'.format(warning_voltage))
                self.updatePushButton.emit(channel_number)
                self.sendMail(channel_number,warning_voltage)
                
    def sendMail(self,channel,warning_voltage):

        fromaddr = "TheBatteries_Rzeszow_Measurement@outlook.com"
        toaddr = "thebatteries_rzeszow_measurement@outlook.com"
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "{0} {1} from {2} data".format(self.sampleID[channel],self.meas_type[channel],self.date_channel[channel])
        filename = 'DAQ_{0}_{1}_{2}.csv'.format(self.sampleID[channel],self.date_channel[channel],self.meas_type[channel])
#        msg['Subject'] = "Measurement"
        if self.meas_type[channel] == 'charge':
            body = "{} reached over {} V during charging".format(self.sampleID[channel],warning_voltage)
        else:
            body = "{} reached under {} V during discharging".format(self.sampleID[channel],warning_voltage)
        msg.attach(MIMEText(body, 'plain'))
        part=MIMEBase('application', "octet-stream")
        part.set_payload(open(filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename=filename)
        msg.attach(part)
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(fromaddr, 'tb2718281828')
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        print("Email with '{}' file has been sent".format(filename))            
    def createDataFrame(self):                                # create data frame with filename
        
        self.df=[]
        for i in range(7):
            self.df.append(pd.DataFrame(columns=['Channel {}'.format(i),'time']))
        # self.df=pd.DataFrame(columns=['Channel %s'%channel_num,'time'])    #
        # self.df.to_csv('DAQ %s.csv' % filename,sep='\t',index_label='Index')                #
        # self.filename = filename
        
    def appendDataFrame(self,channel_num,read_value,read_time,sampleID,date,meas_type,i):
        self.df[channel_num]=self.df[channel_num].append(pd.DataFrame({'Channel {}'.format(channel_num): read_value,'time': read_time},index=[i])).astype(float)    #
                                                                                                                #
        self.df[channel_num].tail(1).to_csv('DAQ_{0}_{1}_{2}.csv'.format(sampleID,date,meas_type),mode='a',sep='\t',header=False)                             #
        return self.df[channel_num]
        
    def measure(self,channel):
        # Use the a_in_32 method for devices with a resolution > 16
        # (optional parameter omitted)
        value = ul.a_in_32(self.board_num, channel, self.ai_range)
        # Convert the raw value to engineering units
        self.eng_units_value = ul.to_eng_units_32(self.board_num, self.ai_range, value)
                       
    
class MyApp(QtGui.QMainWindow):
    workerObject = WorkerObject()
    
    def __init__(self):                                                        #
        super(MyApp,self).__init__()                                        #
        uic.loadUi('daq.ui',self)                                    #
        
        # self.createWorkerThread()
        
        self.show()
        self.pushButton = [self.pushButton_START_ch0,self.pushButton_START_ch1,self.pushButton_START_ch2,
                          self.pushButton_START_ch3,self.pushButton_START_ch4,self.pushButton_START_ch5,self.pushButton_START_ch6]
        self.pushButton[0].clicked.connect(lambda:self.clickButtonEvent_new(0))
        self.pushButton[1].clicked.connect(lambda:self.clickButtonEvent_new(1))
        self.pushButton[2].clicked.connect(lambda:self.clickButtonEvent_new(2))
        self.pushButton[3].clicked.connect(lambda:self.clickButtonEvent_new(3))
        self.pushButton[4].clicked.connect(lambda:self.clickButtonEvent_new(4))
        self.pushButton[5].clicked.connect(lambda:self.clickButtonEvent_new(5))
        self.pushButton[6].clicked.connect(lambda:self.clickButtonEvent_new(6))
#        self.pushButton_START_ch0.clicked.connect(lambda:self.clickButtonEvent_new(0))
#        self.pushButton_START_ch1.clicked.connect(lambda:self.clickButtonEvent_new(1))
#        self.pushButton_START_ch2.clicked.connect(lambda:self.clickButtonEvent_new(2))
#        self.pushButton_START_ch3.clicked.connect(lambda:self.clickButtonEvent_new(3))
#        self.pushButton_START_ch4.clicked.connect(lambda:self.clickButtonEvent_new(4))
#        self.pushButton_START_ch5.clicked.connect(lambda:self.clickButtonEvent_new(5))
#        self.pushButton_START_ch6.clicked.connect(lambda:self.clickButtonEvent_new(6))
        
        self.lineEdit_sampleID_ch0.editingFinished.connect(self.setSampleID)
        self.lineEdit_sampleID_ch1.editingFinished.connect(self.setSampleID)
        self.lineEdit_sampleID_ch2.editingFinished.connect(self.setSampleID)
        self.lineEdit_sampleID_ch3.editingFinished.connect(self.setSampleID)
        self.lineEdit_sampleID_ch4.editingFinished.connect(self.setSampleID)
        self.lineEdit_sampleID_ch5.editingFinished.connect(self.setSampleID)
        self.lineEdit_sampleID_ch6.editingFinished.connect(self.setSampleID)
        
        # self.lineEdit_board_num.editingFinished.connect(updateValue)
        self.lineEdit_charge_warning.editingFinished.connect(self.updateValue)
        self.lineEdit_discharge_warning.editingFinished.connect(self.updateValue)
        self.workerObject.updateLabel.connect(self.updateLabels)
        self.workerObject.updatePushButton.connect(self.updateButtonText)
        # timer.timeout.connect(self.TimeOut)
        
    def updateValue(self):
        sender = self.sender()
        if sender.objectName() == 'lineEdit_charge_warning':
            self.workerObject.warning_charge = float(self.lineEdit_charge_warning.text())
            print('Charge warning value: %s V'%self.workerObject.warning_charge)
        elif sender.objectName() == 'lineEdit_discharge_warning':
            self.workerObject.warning_discharge = float(self.lineEdit_discharge_warning.text())
            print('Discharge warning value: %s V'%self.workerObject.warning_discharge)
        elif sender.objectName() == 'lineEdit_board_num':
            self.workerObject.board_num = int(self.lineEdit_board_num.text())
            print('Board number: %s'%self.workerObject.board_num)
    def setSampleID(self):
        sender = self.sender()
        if sender.objectName() == 'lineEdit_sampleID_ch0':
            self.workerObject.sampleID[0]=self.lineEdit_sampleID_ch0.text()
        elif sender.objectName() == 'lineEdit_sampleID_ch1':
            self.workerObject.sampleID[1]=self.lineEdit_sampleID_ch1.text()
        elif sender.objectName() == 'lineEdit_sampleID_ch2':
            self.workerObject.sampleID[2]=self.lineEdit_sampleID_ch2.text()
        elif sender.objectName() == 'lineEdit_sampleID_ch3':
            self.workerObject.sampleID[3]=self.lineEdit_sampleID_ch3.text()
        elif sender.objectName() == 'lineEdit_sampleID_ch4':
            self.workerObject.sampleID[4]=self.lineEdit_sampleID_ch4.text()
        elif sender.objectName() == 'lineEdit_sampleID_ch5':
            self.workerObject.sampleID[5]=self.lineEdit_sampleID_ch5.text()
        elif sender.objectName() == 'lineEdit_sampleID_ch6':
            self.workerObject.sampleID[6]=self.lineEdit_sampleID_ch6.text()
    def createWorkerThread(self):
    # self.worker = WorkerObject()
        worker_thread = QtCore.QThread()
        self.workerObject.moveToThread(worker_thread)
        self.workerObject.finished.connect(worker_thread.quit)
        worker_thread.started.connect(self.workerObject.trigger_start_new)
        worker_thread.start()
            
    def stopThread(self):
        self.workerObject.stop()
        self.worker_thread.quit()
        self.worker_thread.wait()
        
    def clickButtonEvent_new(self,channel):
        self.workerObject.date_channel[channel] = time.strftime("%Y-%m-%d--%H-%M-%S")
        if self.workerObject.channel_status[channel] == 'OFF':
            self.pushButton[channel].setText('STOP')
            channel_dialog = QtGui.QInputDialog.getText(self, 'Select measurement type', 'Enter type of measurement (charge or discharge): ')
            self.workerObject.meas_type[channel] = str(channel_dialog[0])
            ul.d_bit_out(self.workerObject.board_num, self.workerObject.port.type, channel, 0)
            self.workerObject.channel_status[channel] = 'ON'
            print('Channel {} - measure STARTED'.format(channel))
#            if self.workerObject.started is False:
#                    self.showdialog()
            self.workerObject.trigger_start_new()
#                    print(str(self.workerObject.meas_type[0]))
        else:
            self.pushButton[channel].setText('START')
            self.workerObject.channel_status[channel] = 'OFF'
            print('Channel {} - measure STOPPED'.format(channel))
                      
    def updateLabels(self,channel_num,read_value):
        if channel_num == 0:
#            self.label_ch0_raw.setText(str(self.workerObject.ch0_read))
            self.label_ch0_eng.setText(str(read_value))
        elif channel_num == 1:
            self.label_ch1_eng.setText(str(read_value))
        elif channel_num == 2:
            self.label_ch2_eng.setText(str(read_value))
        elif channel_num == 3:
            self.label_ch3_eng.setText(str(read_value))
        elif channel_num == 4:
            self.label_ch4_eng.setText(str(read_value))
        elif channel_num == 5:
            self.label_ch5_eng.setText(str(read_value))
        elif channel_num == 6:
            self.label_ch6_eng.setText(str(read_value))
    def updateButtonText(self,channel):
        if self.pushButton[channel].text() == 'STOP':
            self.pushButton[channel].setText('START')

            
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()

    sys.exit(app.exec_())
    end_command = raw_input("Press any key to exit")        