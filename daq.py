# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 12:00:14 2018

@author: Roman
"""
import sys
import os
import time
import threading
import shutil
import csv
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import pyqtSlot
import pyqtgraph as pg
import openpyxl
from mcculw import ul
from modules.ai import AnalogInputProps
from modules.digital import DigitalProps
import modules.sendmail as sendmail

def logthread(caller):
    """we check what thread is used while calling"""
    print('%-25s: %s, %s,' % (caller, threading.current_thread().name,
                              threading.current_thread().ident))
class WorkerObject(QtCore.QObject):
    """worker class for handling measurements stuff"""
    updateLabel = QtCore.Signal(int, float)
    updatePushButton = QtCore.Signal(int)
    start = QtCore.Signal()
    finished = QtCore.Signal()
    sendToServer = QtCore.Signal(str)
    update_parameters = QtCore.Signal(int, str, str, str, str, float)
    def __init__(self):                                    #
        QtCore.QObject.__init__(self)
        self.init_DAQ_settings()

        self.channel_number = 0
        self.warning_charge = 4.2
        self.warning_discharge = 3.0
        self.started = False
        self.channel_status = {i:'OFF' for i in range(8)} # status: ON or OFF
        self.meas_type = {}  # 'charge' or 'discharge'
        self.date_channel = {}   # date : 'Y-m-d - H-M-S' - we add it to filename
        self.sampleID = {}
        self.current = {}
        self.filename = {}
        self.start_time = {}
        self.data_dictionary = {}
        self.single_measurement_dictionary = {i: dict.fromkeys(['Voltage',
                                                                'time',
                                                                'Capacity'])
                                              for i in range(8)
                                             }
        self.send_email = False
        self.addressList = []

        self.update_parameters.connect(self.set_parameters)
        self.start.connect(self.run)

    def init_DAQ_settings(self):
        """connect with DAQ USB-2408"""
        self.board_num = 0
        self.ai_props = AnalogInputProps(self.board_num)
        self.ai_range = self.ai_props.available_ranges[0]
        self.digital_props = DigitalProps(self.board_num)
        self.port = next(
                        (port for port in self.digital_props.port_info
                         if port.supports_output), None)
        ul.d_out(self.board_num, self.port.type, 0xFF) #0xFF is port value

    @pyqtSlot()
    def run(self):
        """create QTimer for trigger loop and start it"""
        # logthread('WorkerObject / run  ')
        if not self.started:
            # logthread('WorkerObject / run not started ')                                    #
            self.started = True
            self.loop_timer = QtCore.QTimer(self)
            self.loop_timer.timeout.connect(self.measurement_loop)
            self.loop_timer.start(500)

    def measurement_loop(self):
        """main loop where measurements are gathered and processed"""
        for i in [channel for channel, status in self.channel_status.items() if status == 'ON']:
            read_value = self.measure(self.board_num, i)
            #round time to 2 decimal places:
            read_time = int((time.time() - self.start_time[i])*(10**2))/(10.**2)
            self.update_single_measurement_dictionary(i, read_value, read_time)
            self.update_dictionary_with_measurements(i, self.single_measurement_dictionary[i])
            self.setDigitalOutputVoltage(i, read_value, self.meas_type[i])
            self.updateLabel.emit(i, read_value)
        if not 'ON' in self.channel_status.values():
            self.started = False
            self.loop_timer.stop()

    def update_single_measurement_dictionary(self, channel, voltage_value,
                                             read_time):
        self.single_measurement_dictionary[channel]['Voltage'] = voltage_value
        self.single_measurement_dictionary[channel]['time'] = read_time
        self.single_measurement_dictionary[channel]['Capacity'] = read_time*self.current[channel]/3600

    def setDigitalOutputVoltage(self, channel_number, read_value, meas_type):
        """ We set Digital output to 1 (OFF) to specified channel when
        charge/discharge finishes and send and email"""
        # def sendMail1():
            # print('address list: {}'.format(self.addressList))
            # sendmail.sendMail(self, channel_number, warning_voltage, self.filename[channel_number])
        if meas_type == 'charge':
            warning_voltage = self.warning_charge
            if read_value > warning_voltage:
                # set digital output in specified channel to 0 V:
                ul.d_bit_out(self.board_num, self.port.type, channel_number, 1)
                self.channel_status[channel_number] = 'OFF'
                print(('Channel number - {}:'
                       'Voltage over {} - '
                       'measurement stopped'
                       .format(channel_number, warning_voltage)
                      ))
                self.updatePushButton.emit(channel_number)
                self.sendToServer.emit(self.filename[channel_number])
                if self.send_email:
                    self.send_mail(channel_number, warning_voltage)
        else:
            warning_voltage = self.warning_discharge
            if read_value < warning_voltage:
                ul.d_bit_out(self.board_num, self.port.type, channel_number, 1)
                self.channel_status[channel_number] = 'OFF'
                print(('Channel number - {}: '
                       'Voltage under {} - '
                       'measurement stopped'
                       .format(channel_number, warning_voltage)
                      ))
                self.updatePushButton.emit(channel_number)
                self.sendToServer.emit(self.filename[channel_number])
                if self.send_email:
                    self.send_mail(channel_number, warning_voltage)

    def send_mail(self, channel, warning_voltage):
        """create a new thread to not freeze the reading from device
        and send an email"""
        thread_mail = threading.Thread(target=sendmail.sendMail,
                                       args=(channel, warning_voltage,
                                             self.filename[channel],
                                             self.addressList
                                             )
                                       )
        thread_mail.start()
    def csv_file_create(self, filename):
        """create new .csv file with headers"""
        fieldnames = ['Voltage', 'time', 'Capacity']
        with open(filename, 'w', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
            writer.writeheader()
    def update_dictionary_with_measurements(self, channel,
                                            single_measurement_dictionary):
        """dictionary with all data which is used to plot"""
        for key in self.data_dictionary[channel].keys():
            self.data_dictionary[channel][key].append(single_measurement_dictionary[key])
        self.csv_file_append(single_measurement_dictionary, channel)

    def csv_file_append(self, dict_to_save, channel):
        """append file with single measurement"""
        fieldnames = ['Voltage', 'time', 'Capacity']
        with open(self.filename[channel], 'a', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
            writer.writerow(dict_to_save)

    def measure(self, board_num, channel):
        " read Voltage from DAQ device "
        # Use the a_in_32 method for devices with a resolution > 16
        # (optional parameter omitted)
        value = ul.a_in_32(board_num, channel, self.ai_range)
        # Convert the raw value to engineering units
        eng_units_value = ul.to_eng_units_32(board_num, self.ai_range, value)
        return eng_units_value

    def set_parameters(self, channel, filename, date, sampleID, meas_type, current):
        """everything which is needed to properly mark current measurement is
        defined there"""
        self.filename[channel] = filename
        self.csv_file_create(filename)
        self.date_channel[channel] = date
        self.sampleID[channel] = sampleID
        self.meas_type[channel] = meas_type
        self.current[channel] = current
        self.data_dictionary[channel] = {'Voltage': [],
                                         'time': [],
                                         'Capacity': []
                                        }
        self.start_time[channel] = time.time()

class MyApp(QtWidgets.QMainWindow):
    workerObject = WorkerObject()
    def __init__(self):
        super(MyApp, self).__init__()
        uic.loadUi(os.path.dirname(os.path.abspath(__file__))+"\\daq.ui", self)

        self.initUI()
        self.plot_timer = QtCore.QTimer()
        self.plot_timer.timeout.connect(self.plot_update)

        self.directory = "C:\\Users\\Measurement\\Desktop\\MEASUREMENTS\\"
        self.threadPool = ['']*2
        self.moved = False
        self.channel_to_plot = None
    def initUI(self):
        """connecting every GUI elements there"""
        self.pushButton = (self.pushButton_START_ch0, self.pushButton_START_ch1,
                           self.pushButton_START_ch2, self.pushButton_START_ch3,
                           self.pushButton_START_ch4, self.pushButton_START_ch5,
                           self.pushButton_START_ch6, self.pushButton_START_ch7
                          )
        self.pushButton[0].clicked.connect(lambda: self.button_start_click_event(0))
        self.pushButton[1].clicked.connect(lambda: self.button_start_click_event(1))
        self.pushButton[2].clicked.connect(lambda: self.button_start_click_event(2))
        self.pushButton[3].clicked.connect(lambda: self.button_start_click_event(3))
        self.pushButton[4].clicked.connect(lambda: self.button_start_click_event(4))
        self.pushButton[5].clicked.connect(lambda: self.button_start_click_event(5))
        self.pushButton[6].clicked.connect(lambda: self.button_start_click_event(6))
        self.pushButton[7].clicked.connect(lambda: self.button_start_click_event(7))

        self.pushButton_emailList_add.clicked.connect(self.email_list_add_record)
        self.pushButton_emailList_remove.clicked.connect(self.email_list_remove_record)
        self.comboBox_channelList.activated.connect(self.select_plot_channel)
        self.pushButton_plot.released.connect(self.plot_init)

        self.voltage_label = [self.label_ch0_eng, self.label_ch1_eng,
                              self.label_ch2_eng, self.label_ch3_eng,
                              self.label_ch4_eng, self.label_ch5_eng,
                              self.label_ch6_eng, self.label_ch7_eng
                             ]

        self.sampleID_box = (self.lineEdit_sampleID_ch0, self.lineEdit_sampleID_ch1,
                             self.lineEdit_sampleID_ch2, self.lineEdit_sampleID_ch3,
                             self.lineEdit_sampleID_ch4, self.lineEdit_sampleID_ch5,
                             self.lineEdit_sampleID_ch6, self.lineEdit_sampleID_ch7
                            )

        self.lineEdit_charge_warning.editingFinished.connect(self.update_value)
        self.lineEdit_discharge_warning.editingFinished.connect(self.update_value)
        self.checkBox_send_email.stateChanged.connect(self.update_value)

        self.workerObject.updateLabel.connect(self.update_voltage_label)
        self.workerObject.updatePushButton.connect(self.update_button_text)
        self.workerObject.sendToServer.connect(self.upload_files_to_server)

        self.email_objects_show_and_hide()
        self.show()

    def update_value(self):
        sender = self.sender()
        if sender.objectName() == 'lineEdit_charge_warning':
            self.workerObject.warning_charge = float(self.lineEdit_charge_warning.text())
            print(('Charge warning value: '
                   '{} V'.format(self.lineEdit_charge_warning.text())
                  ))
        elif sender.objectName() == 'lineEdit_discharge_warning':
            self.workerObject.warning_discharge = float(self.lineEdit_discharge_warning.text())
            print(('Discharge warning value: '
                   '{} V'.format(self.lineEdit_discharge_warning.text())
                  ))
        elif sender.objectName() == 'checkBox_send_email':
            self.workerObject.send_email = self.checkBox_send_email.isChecked()
            self.email_objects_show_and_hide()

    def button_start_click_event(self, channel):
        """after pressing START/STOP button we change it's text to opposite
        if we start the measurement the popup window is created where we need to
        provide info such as operation number, measurement type and
        charge/discharge current. If we stop the measurement we shut down digital output
        to 0V and we finish gathering measurements from this channel"""
        if self.workerObject.channel_status[channel] == 'OFF':
            self.update_button_text(channel)
            self.create_popup_window(channel)
        else:
            self.update_button_text(channel)
            self.workerObject.channel_status[channel] = 'OFF'
            # when you press STOP, set digital voltage to 0 V
            ul.d_bit_out(self.workerObject.board_num,
                         self.workerObject.port.type, channel, 1)
            print('Channel {} - measure STOPPED'.format(channel))

    def create_popup_window(self, channel):
        """create popup window after START button is pressed
        create slot for push button inside it and show the window
        when popup is visible, disable every other button in GUI"""
        self.popup = Popup()
        self.popup.pushButton_OK.clicked.connect(lambda: self.init_measurements(channel))
        self.popup.show()
        # self.set_state_of_gui_buttons('disable')

    # def set_state_of_gui_buttons(self, state):
        # if state == 'disable':
        #     for button in self.pushButton:
        #         button.setEnabled(False)
        # else:
        #     for button in self.pushButton:
        #         button.setEnabled(True)

    def init_measurements(self, channel):
        """here we initialize measurements - get things like
        sample ID, date, measurement type, sample number, charge/discharge current
        setting file name, set 5V on DAQ digital output, starting new thread
        creating a record in excel file"""
        self.popup.close()
        # self.set_state_of_gui_buttons('enable')
        ul.d_bit_out(self.workerObject.board_num,
                     self.workerObject.port.type, channel, 0
                     )
        (date, sampleID, current, operation_number,
         meas_type, filename) = self.get_measuring_sample_data(channel)
        self.workerObject.update_parameters.emit(channel, filename, date,
                                                 sampleID, meas_type, current)
        self.workerObject.channel_status[channel] = 'ON'
        print('Channel {} - {} - measure STARTED'.format(channel, meas_type))
        self.excel_logfile_update(channel, filename, date, sampleID,
                                  operation_number, meas_type)
        self.moveThread()

    def get_measuring_sample_data(self, channel):
        """we get informations like date of starting the measurement,
        sampleID, charge/discharge current, measurement type and operation number
        which are passed to worker object in init_measurements function"""
        date = time.strftime("%Y-%m-%d_%H-%M-%S")
        sampleID = str(self.sampleID_box[channel].text())
        current = float(self.popup.lineEdit_current.text()) #charge/discharge current
        operation_number = str(self.popup.lineEdit_operation_number.text())
        if self.popup.radioButton_charge.isChecked():
            meas_type = 'charge'
        else:
            meas_type = 'discharge'
        filename = self.set_file_name(date, operation_number, sampleID, meas_type)
        return date, sampleID, current, operation_number, meas_type, filename

    def moveThread(self):
        """Here we create new QThread to avoid freezing GUI"""
        if not self.moved:
            self.my_thread = QtCore.QThread()
            self.my_thread.start()
            self.workerObject.moveToThread(self.my_thread)
            self.moved = True
        self.workerObject.start.emit()

    def select_plot_channel(self):
        self.channel_to_plot = self.comboBox_channelList.currentIndex()

    def plot_init(self):
        """creates plot window, triggers QTimer which refreshes the plot"""
        self.win = pg.GraphicsWindow("plot")
        self.win.resize(1000, 600)
        self.plt = self.win.addPlot(row=1, col=0, title="Plot",
                                    labels={'left': 'Voltage [V]',
                                            'bottom': 'Capacity [mAh]'
                                            }
                                    )
        self.curve = self.plt.plot(pen='b')

        self.label_cords = pg.LabelItem(justify="right")
        self.win.addItem(self.label_cords)
        self.proxy = pg.SignalProxy(self.plt.scene().sigMouseMoved,
                                    rateLimit=60, slot=self.plot_mouse_moved)

        self.plot_timer.start(600)

    def plot_mouse_moved(self, evt):
        """create label with mouse position on the plot window"""
        self.mousePoint = self.plt.vb.mapSceneToView(evt[0])
        self.label_cords.setText(("<span style='font-size: 8pt; color: white'> "
                                  "x = {:0.4f}, "
                                  "<span style='color: white'> "
                                  "y = {:0.4f} </span>"
                                  .format(self.mousePoint.x(), self.mousePoint.y())
                                 ))

    def plot_update(self):
        try:
            self.curve.setData(self.workerObject.data_dictionary[self.channel_to_plot]['Capacity'],
                               self.workerObject.data_dictionary[self.channel_to_plot]['Voltage'],
                               pen=pg.mkPen('b', width=3)
                               )
        except (KeyError, AttributeError):
            self.plot_timer.stop()
            self.win.close()
            print("DataFrame for channel {} is empty".format(self.channel_to_plot))
        if not self.win.isVisible():
            print('Plot window closed')
            self.plot_timer.stop()

    def set_file_name(self, date, operation_number, sampleID, meas_type):
        """if sampleID is provided function creates a folder with sampleID name
        if it doesn't exist, we set filename with sampleID, date and type of measurement"""
        if sampleID:
            if not os.path.isdir(self.directory+'{}'.format(sampleID)):
                os.mkdir(self.directory+'{}'.format(sampleID))
            directory = self.directory+'{}\\'.format(sampleID)
        else:
            directory = self.directory

        if not operation_number: #either it's empty or 0 we don't add it to filename
            filename = (directory
                        + 'DAQ_{0}_{1}_{2}.csv'.format(sampleID, date, meas_type)
                       )
        else:
            filename = (directory
                        + 'DAQ_{0}_{1}_{2}_{3}.csv'.format(sampleID, date,
                                                           meas_type,
                                                           operation_number)
                        )

        if os.path.isfile(filename + '.csv'):
            i = 1
            filename_temp = filename
            while os.path.isfile(filename_temp + '({})'.format(i) + '.csv'):
                i = i + 1
            filename = filename_temp + '({})'.format(i)
        return filename

    def email_list_add_record(self):
        self.listWidget_emailList.addItem(self.lineEdit_emailList_address.text())
        self.email_address_list_update()
    def email_list_remove_record(self):
        listItems = self.listWidget_emailList.selectedItems()
        if not listItems: return
        for item in listItems:
            self.listWidget_emailList.takeItem(self.listWidget_emailList.row(item))
        self.email_address_list_update()
    def email_address_list_update(self):
        address_list = []
        for index in range(self.listWidget_emailList.count()):
            address_list.append(str(self.listWidget_emailList.item(index).text()))
        self.workerObject.addressList = address_list
    def email_objects_show_and_hide(self):
        if not self.checkBox_send_email.isChecked():
            self.label_13.hide()
            self.listWidget_emailList.hide()
            self.pushButton_emailList_remove.hide()
            self.pushButton_emailList_add.hide()
            self.lineEdit_emailList_address.hide()
            self.label_14.hide()
        else:
            self.label_13.show()
            self.listWidget_emailList.show()
            self.pushButton_emailList_remove.show()
            self.pushButton_emailList_add.show()
            self.lineEdit_emailList_address.show()
            self.label_14.show()

    def update_voltage_label(self, channel_num, read_value):
        self.voltage_label[channel_num].setText(str(read_value))

    def update_button_text(self, channel):
        if self.pushButton[channel].text() == 'STOP':
            self.pushButton[channel].setText('START')
        elif self.pushButton[channel].text() == 'START':
            self.pushButton[channel].setText('STOP')

    def excel_logfile_update(self, channel, full_path, date,
                             sampleID, operation_number, meas_type):
        " we append excel log file there"
         # name of the last folder in directory:
        dir_lastfolder = os.path.basename(os.path.dirname(full_path))
        # name of last file in folder, we have to remove it from 'dir' variable:
        file_name = os.path.basename(os.path.normpath(full_path))
        date = (date[0:10].replace('-', '.')
                + date[10].replace('_', " ")
                + date[11:19].replace('-', ':')
               )
        if dir_lastfolder == 'MEASUREMENTS':
            dir_to_print = "\\MEASUREMENTS"
        else:
            #if our .csv log is in, for example: C:\\Folder1\\TestSample\\abc.csv
            #in Excel log file, under location we'll see Folder1\\TestSample
            sampleID_folder = os.path.dirname(full_path).split('\\')[-1]
            base_folder = os.path.dirname(full_path).split('\\')[-2]
            dir_to_print = base_folder + '\\' + sampleID_folder
        hyperlink = '=HYPERLINK("{}", "{}")'.format(os.path.dirname(full_path),
                                                    dir_to_print
                                                   )
        try:
            worksheet_path = ("C:\\Users\\Measurement\\Desktop\\MEASUREMENTS\\"
                              "Measurements_LogFile.xlsx")
            book = openpyxl.load_workbook(worksheet_path)
            sheet = book.active
            sheet.append([sampleID, operation_number,
                          'Board//Channel {}'.format(channel), date, meas_type,
                          file_name, hyperlink]
                        )
            book.save(worksheet_path)
        except IOError:
            print(("CAN'T APPEND CURRENT MEASUREMENT TO LOG FILE - "
                   "PROBABLY IT'S OPENED IN EXCEL"))

    def upload_files_to_server(self, filename):
        try:
            filename = str(filename)
            server_address = '//192.168.0.250//roman//Measurements'

            excelFile_path = ("C:\\Users\\Measurement\\Desktop\\MEASUREMENTS\\"
                              "Measurements_LogFile.xlsx"
                             )

            server_folder = (server_address
                             + '//'
                             + os.path.basename(os.path.dirname(filename))
                             + '//'
                            )

            server_file_path = (server_folder
                                + os.path.basename(os.path.normpath(filename))
                               )
            if not os.path.exists(server_folder):
                os.makedirs(server_folder)
            print(('Creating copy of measurement log on the server, '
                   'directory: {}'.format(server_file_path)))
            shutil.copy2(filename, server_file_path)
            shutil.copy2(excelFile_path, server_address)
            print("Files copied")
        except OSError as e:
            print(e)

class Popup(QtWidgets.QWidget):
    """QWidget class for popup window when START button is pressed
    created for later development but it's already working"""
    def __init__(self):                                                        #
        super(Popup, self).__init__()                                        #
        uic.loadUi(os.path.dirname(os.path.abspath(__file__)) + "\\popup.ui", self)
        # self.hide()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()

    sys.exit(app.exec_())
